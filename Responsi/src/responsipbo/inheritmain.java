package responsipbo;

class inheritmain {

	public static void main(String[] args) {
		student mahasiswa = new student(); //pemanggilan method dari superclass
	
		//pemanggilan method dari subclass 
		//mahasiswa.nim = "A11.2000.00001";
		mahasiswa.input();
		mahasiswa.hitungpembayaran();
		System.out.println();
		mahasiswa.identity();
		//hobby mhs = new hobby(bersepeda)
		mahasiswa.hobby("bersepeda");
	}
}