package responsipbo;

import java.util.Scanner;

class person {

	// atribut dan method super class
	protected String name;
	protected String address;
	
	public void identity()
	{
		System.out.println("Nama: "+name);
		System.out.println("Alamat: "+address);
	}
	
	public void hobby(String hobi)
	{
		System.out.println("Hobi : " + hobi);
	}

}
